// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MazeElement.h"
#include "FunctionalLibruary.h"
#include "practicalGameModeBase.generated.h"

class FSpawnSectionTask
{

public:
    int PositionX = 0;
    int PositionY = 0;

    int SectionIndex;
    TSubclassOf<class AMazeElement>* BP_MazeElement;
    UWorld* myWorld = nullptr;
    TArray<TArray<FCellInfo>> generatedMaze;

    FSpawnSectionTask(int InPositionX, TSubclassOf<class AMazeElement>* InBP_MazeElement, UWorld* InMyWorld, TArray<TArray<FCellInfo>> InGeneratedMaze) :
        PositionX(InPositionX),
        BP_MazeElement(InBP_MazeElement),
        myWorld(InMyWorld),
        generatedMaze(InGeneratedMaze)
    {
        // Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
    }
    ~FSpawnSectionTask()
    {
        // you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
    }
    FORCEINLINE TStatId GetStatId() const
    {
        RETURN_QUICK_DECLARE_CYCLE_STAT(FSpawnSectionTask, STATGROUP_TaskGraphTasks);
    }

    static ENamedThreads::Type GetDesiredThread()
    {
        return ENamedThreads::GameThread;
    }

    static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }

    void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
    {
        for (TArray<FCellInfo> cellsArr : generatedMaze)
        {
            for (FCellInfo cellInfo : cellsArr)
            {
                if (myWorld && BP_MazeElement)
                {
                    FActorSpawnParameters SpawnParameters;
                    SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
                    FVector SpawnLoc = FVector(PositionX, PositionY, 0.0f);
                    PositionY -= 400.0f;
                    FRotator SpawnRot;
                    AMazeElement* SpawnedMazeElement = Cast<AMazeElement>(myWorld->SpawnActor(*BP_MazeElement, &SpawnLoc, &SpawnRot, SpawnParameters));
                    if (SpawnedMazeElement)
                    {
                        SpawnedMazeElement->ConstructCell(cellInfo);
                    }
                }
            }
            PositionX += 400;
            PositionY = 0;
        }
    }
};

class FGenerateSectionTask
{

public:
	int Rows;
	int Columns;
	int SectionIndex;
	TSubclassOf<class AMazeElement>* BP_MazeElement;
	UWorld* myWorld = nullptr;
	bool bIsFirstSection = false;
	bool bIsLastSection = false;

    int PositionX = 0;
    int PositionY = 0;

	FGenerateSectionTask(int InRows, int InColumns, int InSectionIndex, 
		TSubclassOf<class AMazeElement>* InBP_MazeElement, UWorld* InMyWorld, bool InFirstSection, bool InLastSection) :
		Rows(InRows), 
		Columns(InColumns),
		SectionIndex(InSectionIndex),
		BP_MazeElement(InBP_MazeElement),
		myWorld(InMyWorld),
		bIsFirstSection(InFirstSection),
		bIsLastSection(InLastSection)
	{
		// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FGenerateSectionTask()
	{
		// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FGenerateSectionTask, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority myTaskPriority(
			TEXT("FGenerateAccessories_Task"),
			TEXT("FGenerateAccessories_Task in work"),
			ENamedThreads::BackgroundThreadPriority,
			ENamedThreads::NormalTaskPriority,
			ENamedThreads::NormalTaskPriority
		);
		return myTaskPriority.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
        TArray<TArray<FCellInfo>> generatedMaze;
        TArray<FCellInfo> rowsArr;

        for (int j = 0; j < Rows; j++)
        {
            if (j == 0)
            {
                for (int i = 0; i < Columns; i++)
                {
                    FCellInfo newCellInfo;

                    if (i > 0)
                    {
                        if (FMath::RandBool())
                        {
                            newCellInfo.cellIndex = rowsArr[(i - 1)].cellIndex;
                        }
                        else
                        {
                            int bottomBordersNum = 0;

                            newCellInfo.cellIndex = i;
                            rowsArr[(i - 1)].bHaveRightBorder = true;
                        }
                    }
                    else
                    {
                        if (j == 0) newCellInfo.bHaveLeftBorder = true;
                        else rowsArr[i].bHaveLeftBorder = true;
                    }

                    if (j == 0)
                    {
                        if (i == (Columns - 1)) newCellInfo.bHaveRightBorder = true;
                        rowsArr.Add(newCellInfo);
                    }
                    else
                    {
                        rowsArr[i].bHaveTopBorder = false;
                        if (i == (Columns - 1)) rowsArr[i].bHaveRightBorder = true;
                    }
                }

                // draw the lower borders
                for (int h = 1; h < rowsArr.Num(); h++)
                {
                    if (rowsArr[h].cellIndex == rowsArr[(h - 1)].cellIndex)
                    {
                        if (FMath::RandBool()) rowsArr[h].bHaveBottomtBorder = true;
                        else rowsArr[(h - 1)].bHaveBottomtBorder = true;
                    }
                }

                generatedMaze.Add(rowsArr);
            }
            else
            {
                for (int i = 0; i < Columns; i++)
                {
                    rowsArr[i].bHaveTopBorder = false;
                    if (i != (Columns - 1)) rowsArr[i].bHaveRightBorder = false;
                    if (rowsArr[i].bHaveBottomtBorder) rowsArr[i].cellIndex = i;
                    rowsArr[i].bHaveBottomtBorder = false;

                    if (i > 0)
                    {
                        if (FMath::RandBool()) rowsArr[i].cellIndex = rowsArr[(i - 1)].cellIndex;
                        else
                        {
                            rowsArr[i].cellIndex = i;
                            rowsArr[(i - 1)].bHaveRightBorder = true;
                        }
                    }
                }

                // draw the lower borders
                for (int h = 1; h < rowsArr.Num(); h++)
                {
                    if (rowsArr[h].cellIndex == rowsArr[(h - 1)].cellIndex)
                    {
                        if (FMath::RandBool()) rowsArr[h].bHaveBottomtBorder = true;
                        else rowsArr[(h - 1)].bHaveBottomtBorder = true;
                    }
                }

                generatedMaze.Add(rowsArr);
            }
        }

        if (bIsFirstSection)
        {
            for (int i = 0; i < generatedMaze[0].Num(); i++)
            {
                generatedMaze[0][i].bHaveTopBorder = true;
                if (generatedMaze[0][i].cellIndex != (generatedMaze[0].Num()))
                {
                    generatedMaze[0][i].bHaveRightBorder = false;
                }
            }
        }

        if (bIsLastSection)
        {
            for (int i = 0; i < generatedMaze[(generatedMaze.Num() - 1)].Num(); i++)
            {
                generatedMaze[(generatedMaze.Num() - 1)][i].bHaveBottomtBorder = true;
                if (i == 0)
                {
                    generatedMaze[(generatedMaze.Num() - 1)][i].bHaveLeftBorder = false;
                }
                if (i < generatedMaze[(generatedMaze.Num() - 1)].Num() - 1)
                {
                    generatedMaze[(generatedMaze.Num() - 1)][i].bHaveRightBorder = false;
                }
            }
        }

        PositionX = (SectionIndex * Rows) * 400;

        TGraphTask<FSpawnSectionTask>::CreateTask(NULL, ENamedThreads::GameThread).ConstructAndDispatchWhenReady(PositionX, BP_MazeElement, myWorld, generatedMaze);
	}
};

UCLASS()
class PRACTICAL_API ApracticalGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable)
    void GenerateMazeMatrix(const int Columns, const int Rows);

    UFUNCTION(BlueprintCallable)
    void GenerateMaze(int Rows, int Columns, int SectionsNum);

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<class AMazeElement> BP_MazeElement;

    int sizeY = 0;
    int sizeX = 0;
};
