// Copyright Epic Games, Inc. All Rights Reserved.


#include "practicalGameModeBase.h"
#include "vector"

void ApracticalGameModeBase::GenerateMazeMatrix(const int Columns, const int Rows)
{
    TArray<TArray<FCellInfo>> generatedMaze;
    TArray<FCellInfo> rowsArr;

    for (int j = 0; j < Rows; j++)
    {
        if (j == 0)
        {
            for (int i = 0; i < Columns; i++)
            {
                FCellInfo newCellInfo;

                if (i > 0)
                {
                    if (FMath::RandBool())
                    {
                        newCellInfo.cellIndex = rowsArr[(i - 1)].cellIndex;
                    }
                    else
                    {
                        int bottomBordersNum = 0;

                        newCellInfo.cellIndex = i;
                        rowsArr[(i - 1)].bHaveRightBorder = true;
                    }
                }
                else
                {
                    if (j == 0) newCellInfo.bHaveLeftBorder = true;
                    else rowsArr[i].bHaveLeftBorder = true;
                }

                if (j == 0)
                {
                    if (i == (Columns - 1)) newCellInfo.bHaveRightBorder = true;
                    rowsArr.Add(newCellInfo);
                }
                else
                {
                    rowsArr[i].bHaveTopBorder = false;
                    if (i == (Columns - 1)) rowsArr[i].bHaveRightBorder = true;
                }
            }

            // draw the lower borders
            for (int h = 1; h < rowsArr.Num(); h++)
            {
                if (rowsArr[h].cellIndex == rowsArr[(h - 1)].cellIndex)
                {
                    if (FMath::RandBool()) rowsArr[h].bHaveBottomtBorder = true;
                    else rowsArr[(h - 1)].bHaveBottomtBorder = true;
                }
            }

            generatedMaze.Add(rowsArr);
        }
        else
        {
            for (int i = 0; i < Columns; i++)
            {
                rowsArr[i].bHaveTopBorder = false;
                if (i != (Columns - 1)) rowsArr[i].bHaveRightBorder = false;
                if (rowsArr[i].bHaveBottomtBorder) rowsArr[i].cellIndex = i;
                rowsArr[i].bHaveBottomtBorder = false;

                if (i > 0)
                {
                    if (FMath::RandBool()) rowsArr[i].cellIndex = rowsArr[(i - 1)].cellIndex;
                    else
                    {
                        rowsArr[i].cellIndex = i;
                        rowsArr[(i - 1)].bHaveRightBorder = true;
                    }
                }
            }

            // draw the lower borders
            for (int h = 1; h < rowsArr.Num(); h++)
            {
                if (rowsArr[h].cellIndex == rowsArr[(h - 1)].cellIndex)
                {
                    if (FMath::RandBool()) rowsArr[h].bHaveBottomtBorder = true;
                    else rowsArr[(h - 1)].bHaveBottomtBorder = true;
                }
            }

            generatedMaze.Add(rowsArr);
        }
    }
    
    for (TArray<FCellInfo> cellsArr : generatedMaze)
    {
        for (FCellInfo cellInfo : cellsArr)
        {
            UWorld* myWorld = GetWorld();
            if (myWorld && BP_MazeElement)
            {
                FVector SpawnLoc = FVector(sizeX, sizeY, 0.0f);
                sizeY -= 400.0f;
                FRotator SpawnRot;
                AMazeElement* SpawnedMazeElement = Cast<AMazeElement>(myWorld->SpawnActor(BP_MazeElement, &SpawnLoc, &SpawnRot, FActorSpawnParameters()));
                if (SpawnedMazeElement)
                {
                    SpawnedMazeElement->ConstructCell(cellInfo);
                }
            }
        }
        sizeX += 400;
        sizeY = 0;
    }
}

void ApracticalGameModeBase::GenerateMaze(int Rows, int Columns, int SectionsNum)
{
    UWorld* myWorld = GetWorld();
    for (int i = 0; i < SectionsNum; i++)
    {
        if (i == 0)
        {
            TGraphTask<FGenerateSectionTask>::CreateTask(NULL, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(Rows, Columns, i, &BP_MazeElement, myWorld, true, false);
        }
        else if (i == (SectionsNum - 1))
        {
            TGraphTask<FGenerateSectionTask>::CreateTask(NULL, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(Rows, Columns, i, &BP_MazeElement, myWorld, false, true);
        }
        else
        {
            TGraphTask<FGenerateSectionTask>::CreateTask(NULL, ENamedThreads::AnyThread).ConstructAndDispatchWhenReady(Rows, Columns, i, &BP_MazeElement, myWorld, false, false);
        }
    }
}