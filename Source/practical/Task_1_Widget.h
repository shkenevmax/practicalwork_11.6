// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Task_1_Widget.generated.h"

class FGenerateNumsTask
{
	
public:
	int* NumToDo = nullptr;

	FGenerateNumsTask(int* InNum) : NumToDo(InNum) // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
	{
		// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FGenerateNumsTask()
	{
		// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FGenerateNumsTask, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority myTaskPriority(
			TEXT("FGenerateAccessories_Task"),
			TEXT("FGenerateAccessories_Task in work"),
			ENamedThreads::BackgroundThreadPriority,
			ENamedThreads::NormalTaskPriority,
			ENamedThreads::NormalTaskPriority
		);
		return myTaskPriority.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		int numTmp = FMath::RandRange(0, 255);
		*NumToDo = numTmp;
	}
};

class FGenerateColorsTask
{

public:
	FColor* ColorToDo;

	FGenerateColorsTask(FColor* InColor) : ColorToDo(InColor)  // CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
	{
		// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FGenerateColorsTask()
	{
		// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FGenerateColorsTask, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority myTaskPriority(
			TEXT("FGenerateAccessories_Task"),
			TEXT("FGenerateAccessories_Task in work"),
			ENamedThreads::BackgroundThreadPriority,
			ENamedThreads::NormalTaskPriority,
			ENamedThreads::NormalTaskPriority
		);
		return myTaskPriority.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		FColor colorTmp = FColor(FMath::RandRange(0, 255), FMath::RandRange(0, 255), FMath::RandRange(0, 255));
		*ColorToDo = colorTmp;
	}
};

class FGenerateWidgetTask
{

public:
	int* NumToDo = nullptr;
	FColor* ColorToDo = nullptr;

	FGenerateWidgetTask(int* InNum, FColor* InColor) :
		NumToDo(InNum),
		ColorToDo(InColor)// CAUTION!: Must not use references in the constructor args; use pointers instead if you need by reference
	{
		// Usually the constructor doesn't do anything except save the arguments for use in DoWork or GetDesiredThread.
	}
	~FGenerateWidgetTask()
	{
		// you will be destroyed immediately after you execute. Might as well do cleanup in DoWork, but you could also use a destructor.
	}
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(FGenerateWidgetTask, STATGROUP_TaskGraphTasks);
	}

	static ENamedThreads::Type GetDesiredThread()
	{
		FAutoConsoleTaskPriority myTaskPriority(
			TEXT("FGenerateAccessories_Task"),
			TEXT("FGenerateAccessories_Task in work"),
			ENamedThreads::BackgroundThreadPriority,
			ENamedThreads::NormalTaskPriority,
			ENamedThreads::NormalTaskPriority
		);
		return myTaskPriority.Get();
	}

	static ESubsequentsMode::Type GetSubsequentsMode() { return ESubsequentsMode::FireAndForget; }

	void DoTask(ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent)
	{
		TGraphTask<FGenerateNumsTask>::CreateTask(NULL, CurrentThread).ConstructAndDispatchWhenReady(NumToDo);
		TGraphTask<FGenerateColorsTask>::CreateTask(NULL, CurrentThread).ConstructAndDispatchWhenReady(ColorToDo);
	}
};

UCLASS()
class PRACTICAL_API UTask_1_Widget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int NewNum = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor NewColor = FColor(0, 0, 0);

	UFUNCTION(BlueprintCallable)
	void InitRandom();

	UFUNCTION(BlueprintCallable)
	void GenerateMaze(int Rows, int Columns, int Sections);
};
