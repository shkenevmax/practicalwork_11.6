// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalLibruary.generated.h"

USTRUCT(BlueprintType)
struct FCellInfo
{
    GENERATED_BODY()

    UPROPERTY(BlueprintReadWrite)
    int cellIndex = 0;
    UPROPERTY(BlueprintReadWrite)
    bool bHaveLeftBorder = false;
    UPROPERTY(BlueprintReadWrite)
    bool bHaveRightBorder = false;
    UPROPERTY(BlueprintReadWrite)
    bool bHaveTopBorder = false;
    UPROPERTY(BlueprintReadWrite)
    bool bHaveBottomtBorder = false;
};

class PRACTICAL_API FunctionalLibruary
{
public:
	


};
